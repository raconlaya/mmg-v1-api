<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('zones', function($table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('width');
            $table->integer('height');
            $table->integer('position_top');
            $table->integer('position_left');
            $table->timestamps();
        });
        Schema::create('plant_definitions', function($table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('family');
            $table->text('description');
            $table->timestamps();
        });
        Schema::create('plants', function($table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('cultivar')->nullable();
            $table->unsignedBigInteger('plant_definition_id')->nullable();
            $table->foreign('plant_definition_id')->references('id')->on('plant_definitions')->onDelete('cascade');
            $table->bigInteger('zone_id')->unsigned()->nullable();
            $table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');

            $table->timestamps();
        });
        Schema::create('task_definition_types', function($table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('task_definitions', function($table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('category');
            $table->text('description');
            $table->date('date_beg')->nullable();
            $table->date('date_end')->nullable();
            $table->bigInteger('plant_definition_id')->unsigned()->nullable();
            $table->foreign('plant_definition_id')->references('id')->on('plant_definitions')->onDelete('cascade');
            
            $table->bigInteger('task_definition_type_id')->unsigned()->nullable();
            $table->foreign('task_definition_type_id')->references('id')->on('task_definition_types')->onDelete('set null');
            $table->timestamps();
        });
        Schema::create('tasks', function($table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('date_beg')->nullable();
            $table->date('date_end')->nullable();
            $table->unsignedBigInteger('task_definition_id')->nullable();
            $table->foreign('task_definition_id')->references('id')->on('task_definitions')->nullOnDelete();
            $table->unsignedBigInteger('plant_id')->nullable();
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('cascade');
            $table->text('description');
            $table->unsignedBigInteger('zone_id')->nullable();
            $table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('task_definitions');
        Schema::dropIfExists('plants');
        Schema::dropIfExists('plant_definitions');
        Schema::dropIfExists('zones');
        Schema::dropIfExists('task_definition_types');
    }
};
