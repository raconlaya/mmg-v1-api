<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StorePlantFamilyRequest;
use App\Http\Requests\Api\UpdatePlantFamilyRequest;
use App\Http\Resources\PlantFamilyResource;
use App\Http\Resources\PlantFamilyCollection;
use App\Models\PlantFamily;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class PlantFamilyApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/plant-family",
     *      operationId="listPlantFamily",
     *      tags={"PlantFamily"},
     *      summary="List all plant families",
     *      description="Returns plant families data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        //abort_if(Gate::denies('project_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PlantFamilyCollection(PlantFamily::all());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/plant-family",
     *      operationId="storePlantFamily",
     *      tags={"PlantFamily"},
     *      summary="Store new plant family",
     *      description="Returns plant family data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name"},
     *              @OA\Property(property="name", type="string", example="Solanaceae")
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StorePlantFamilyRequest $request)
    {
        $project = PlantFamily::create($request->all());

        return (new PlantFamilyResource($project))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/plant-family/{id}",
     *      operationId="getPlantFamilyById",
     *      tags={"PlantFamily"},
     *      summary="Get plant family information",
     *      description="Returns plant family data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant family id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"name": "Solanaceae", "created_at": "2023-03-12T10:30:22.000000Z", "updated_at": "2023-03-12T10:30:22.000000Z"}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show record $id");
        // abort_if(Gate::denies('project_show'),   Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant_family = PlantFamily::findOrFail($id);
        return (new PlantFamilyResource($plant_family))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/plant-family/{id}",
     *      operationId="updatePlantFamily",
     *      tags={"PlantFamily"},
     *      summary="Update existing plant family",
     *      description="Returns updated plant family data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant family id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name"},
     *              @OA\Property(property="name", type="string", example="Solanaceae"),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdatePlantFamilyRequest $request) // , string $id)
    {
        Log::debug("Udpate record $id");
        $plant_family = PlantFamily::findOrFail($id);
        $plant_family->update($request->all());

        return (new PlantFamilyResource($plant_family))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/plant-family/{id}",
     *      operationId="deletePlantFamily",
     *      tags={"PlantFamily"},
     *      summary="Delete existing plant family",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant family id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(String $id)
    {
        // abort_if(Gate::denies('project_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant_family = PlantFamily::find($id);
        $http_code =  Response::HTTP_NOT_FOUND;
        if(!empty($plant_family)) {
            $plant_family->delete();
            $http_code =  Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}