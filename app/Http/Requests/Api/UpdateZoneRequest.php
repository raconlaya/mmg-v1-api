<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ZoneRequest;

class UpdateZoneRequest extends ZoneRequest
{
    use ApiRequestTrait;
}
