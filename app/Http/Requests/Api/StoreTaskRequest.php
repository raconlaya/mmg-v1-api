<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskRequest;
class StoreTaskRequest extends TaskRequest
{
    use ApiRequestTrait;
}
