<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\PlantDefinitionRequest;

class StorePlantDefinitionRequest extends PlantDefinitionRequest
{
    use ApiRequestTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true; // FIXME : backpack_auth()->check() doew not
    }
}
