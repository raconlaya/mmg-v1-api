<?php
namespace App\Models\Repositories;

use App\Models\Plant;
use App\Models\Zone;

class PlantRepository extends BaseRepository
{
    public function __construct(Plant $model)
    {
        parent::__construct($model);
    }

    public function getByZone(Zone $zone) {
        return $this->model->where('zone_id', $zone->id)->get();
    }
}
